# Pronounce

Bash script to pronounce words by fetching dictionary.com's 'ogg' file and playing it back with mplayer.

# Requirements

- wget
- mplayer
- Sound card
- And these commands: grep, head, awk.
