#!/bin/bash

# Chcking for binaries
FETCH="curl"
FETCH_P="--silent"
hash ${FETCH} 2>/dev/null || { echo >&2 "${FETCH} wasn't found..."; exit 100; }
PLAY="mplayer"
hash ${PLAY} 2>/dev/null || { echo >&2 "${PLAY} wasn't found..."; exit 100; }

WORD="$1"
if [ -z "$1" ]; then
  echo "Usage: $0 WORD-TO-PRONOUNCE"
  exit 200
fi

echo -en "Powered by Dictionary.com\n\n"

URL=$(${FETCH} ${FETCH_P} http://www.dictionary.com/browse/${WORD}|grep -nw "ogg"|head -n 1|awk -F"\" type=\"audio/ogg\">" '{print $1}'|awk -F"<source src=\"" '{print $2}')
if [ -z "$URL" ]; then
  echo "Couldn't find/fetch ${WORD}"
  exit 300
else
  echo "Saying: \"${WORD}\"..."
  ${PLAY} ${URL} > /dev/null 2>&1
fi
